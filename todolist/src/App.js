import "./Assests/CSS/Style.scss";
import Addtodo from "./Components/Addtodo/Addtodo";
import Displaytodo from "./Components/Displaytodo/Displaytodo";
import TodoProvider from "./Context/TodoContext";



function App() {
  return (

    <TodoProvider>
      <div className="App">
        <Addtodo />
        <Displaytodo />
      </div>
    </TodoProvider>
  );
}

export default App;
