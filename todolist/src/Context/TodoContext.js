import { createContext } from "react";
import { useState } from "react";

 export const TodoContext = createContext();

export default function TodoProvider({children}){
    const [todos, setTodos] =  useState([]);

    return(
        <TodoContext.Provider value={{todos, setTodos}}>
            {children}
        </TodoContext.Provider>
    )
}
