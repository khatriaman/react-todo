import React, { useEffect } from "react";
import edit from "../../Assests/Images/pen-to-square-solid.svg";
import remove from "../../Assests/Images/trash-solid.svg";
import { useState, useContext } from "react";
import { TodoContext } from "../../Context/TodoContext";
import { Select } from "@material-ui/core";

function Todo({ task, id, status, removeTodo, editTodo, select }) {
  const { todos, setTodos } = useContext(TodoContext);
  const [isEdit, setIsEdit] = useState(false);
  const [editText, setEditText] = useState("");


  const markDone = (e) => {
    setTodos(
      todos.map((todo) => {
        let checkbox = e.target;
        if (todo.id == checkbox.id) {
          if (checkbox.checked) {
            return { ...todo, status: "done" };
          } else {
            return { ...todo, status: "active" };
          }
        }
        return todo;
      })
    );
    console.log(todos);
  };

  const EditTodo = (e) => {
    setIsEdit(true);
  };
  const editDone = (e) => {
    setTodos(
      todos.map((todo) => {
        if (todo.id == e.target.id) {
          return { ...todo, Task: editText };
        }
        return todo;
      })
    );
    setIsEdit(false);
    setEditText("");
  };

  if (isEdit) {
    return (
      <div className="Todo">
        <div className="container-fluid">
          <div className="Edit-wrapper">
            <input
              type="text"
              value={editText}
              onChange={(e) => setEditText(e.target.value)}
            />
            <button id={id} onClick={(e) => editDone(e)} className="btn_edit">
              done
            </button>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="Todo">
        <div className="container-fluid">
          <div className="task-wrapper">
            <div className="task">
              <input
                type="checkbox"
                className="check"
                onClick={(e) => {
                  markDone(e);
                }}
                id={id}
              />
              <h4>{task}</h4>
              <div className="icons-wrapper">
                <img
                  src={edit}
                  className={select == "done" ? "icon_edit" : "icon"}
                  alt="edit-icon"
                  id={id}
                  onClick={(e) => EditTodo(e)}
                />
                <img
                  src={remove}
                  className="icon"
                  id={id}
                  alt="remove-icon"
                  onClick={(e) => removeTodo(e)}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Todo;
