import React, {useContext, useEffect, useState} from 'react';
import Todo from '../Todo/Todo';
import { TodoContext } from '../../Context/TodoContext';




function Displaytodo() {

    const { todos, setTodos } = useContext(TodoContext);
    const [select, setSelect] = useState("")
    const [displayTodos, setDisplayTodos] = useState([]);

    useEffect(() => {
        setDisplayTodos(todos);
    }, [todos])
    
    const removeTodo = (e) => {
       const newtodos = todos.filter(todo => todo.id != e.target.id)
       console.log(newtodos)
       setTodos(newtodos)
    }

    const editTodo = (e) => {
        
    }   

    const filterTodo = (e) => {
        setDisplayTodos(todos);
        let allBtns = document.querySelectorAll(".btn")
        allBtns.forEach(btn => {
            btn.classList.remove("selected")
        })
        let elem = e.target;
        elem.classList.add("selected")
        console.log(e.target)
        if(e.target.id === "active"){
            const activeTodos = todos.filter(todo => todo.status === "active")
            console.log(activeTodos)
            setDisplayTodos(activeTodos)
            setSelect("active")
        }
        else if(e.target.id === "done"){
            const doneTodos = todos.filter(todo => todo.status === "done")
            console.log(doneTodos);
            setDisplayTodos(doneTodos);
            setSelect("done")
        }else{
            setDisplayTodos(todos);
            setSelect("all")
        }
    }

        return (
            <div className='Displaytodo'>
                <div className='container-fluid'>
                    <div className='filter-btns'>
                        <button className='btn' id='all' onClick={(e) => filterTodo(e)}>All</button>
                        <button className='btn' id='active'onClick={(e) => filterTodo(e)}>Active</button>
                        <button className='btn' id='done' onClick={(e) => filterTodo(e)}>Done</button>
                    </div>
                   
                    {displayTodos.map((todo, index) => {
                        return <Todo key={index} task={todo.Task} id={todo.id} status={todo.status} removeTodo={removeTodo} editTodo={editTodo} select={select}/>
                    })}
                </div>
            </div> 
        )
    
}

export default Displaytodo