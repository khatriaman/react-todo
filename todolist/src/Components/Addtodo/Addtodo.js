import React, { useContext } from "react";
import { useState } from "react"
import { TodoContext } from "../../Context/TodoContext";

  
function Addtodo() {
  const [text, setText] = useState("");
  var [id, setId] = useState(0);

  const { todos, setTodos } = useContext(TodoContext);

  

  const addtodo = () => {
      if(text){
        setTodos([...todos, { Task : text, id : id, status : "active" }])
        setText("");
        setId(++id);
      }
      else{
          alert("Text Cannot be empty");
      }
    
      

  }
  return (
    <div className="Addtodo">
      <div className="container-fluid">
        <h1>What needs to be done?</h1>
        <input
        className="todo-input"
        placeholder="Add Your Todo Here"
          typeof="text"
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <button className="btn_addtodo" onClick={addtodo}>Add</button>
      </div>
    </div>
  );
}

export default Addtodo;
